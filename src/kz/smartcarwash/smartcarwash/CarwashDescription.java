package kz.smartcarwash.smartcarwash;

import kz.smartcarwash.supplementaryClasses.ImageAdapter;
import kz.smartcarwash.supplementaryClasses.RunScript;
import kz.smartcarwash.userData.User;

import org.json.JSONException;
import org.json.JSONObject;

import ru.yandex.yandexmapkit.MapController;
import ru.yandex.yandexmapkit.MapView;
import ru.yandex.yandexmapkit.OverlayManager;
import ru.yandex.yandexmapkit.overlay.Overlay;
import ru.yandex.yandexmapkit.overlay.OverlayItem;
import ru.yandex.yandexmapkit.utils.GeoPoint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.TextView;

public class CarwashDescription extends Activity implements OnClickListener {
	
	private int id=0;
	private JSONObject data;
	private Gallery gallery;
	private String urls[]=new String[3];
	private TextView phone, description,name,price;
	private OverlayItem driver,from,to;
	private Overlay overlay;
	private OverlayManager mOverlayManager;
	private Double latitude,longitude;
	private MapController mMapController;
	private MapView map;
	private Button book;
	private User U;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.carwash_description);
		
		U=User.getInstance(getParent());
		gallery=(Gallery) findViewById(R.id.cw_description_gallery);
		phone= (TextView) findViewById(R.id.cw_description_tel);
		description=(TextView) findViewById(R.id.cw_description_description);
		name=(TextView) findViewById(R.id.cw_description_name);
		price=(TextView) findViewById(R.id.cw_description_avgprice);
		book=(Button) findViewById(R.id.cw_description_book);
		
		id=getIntent().getExtras().getInt("id");
		
		book.setOnClickListener(this);
		
	
		
		JSONObject jo=new JSONObject();
		try {
			jo.put("id", id);
			RunScript RS=new RunScript("http://www.smartcarwash.kz/php/wash_detailed.php", jo);
			data=new JSONObject(RS.getResult());
			if(data.getInt("status")==1){
				
				data= data.getJSONObject("message");
				
				Log.d("mylog",data.toString());
				
				urls[0]=data.getString("img1");
				urls[1]=data.getString("img2");
				urls[2]=data.getString("img3");
				
				Log.d("mylog","urls done");
				
				gallery.setAdapter(new ImageAdapter(this, urls));
				
				Log.d("mylog","gallery done");
				
				
				phone.setText(data.getString("telephone"));
				Log.d("mylog","phone done");
				description.setText(data.getString("description"));
				name.setText(data.getString("name"));
				price.setText(data.getString("avgprice")+" тг.");
				
				Log.d("mylog","des done");
				
				manageMap();
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		book.setVisibility(book.INVISIBLE);
		
		if (U.is_logged()){
			if (U.getRole()==1){
				book.setVisibility(book.VISIBLE);
			}
		}
		super.onResume();
	}
	
	private void manageMap(){
		map=(MapView) findViewById(R.id.client_map);
		mMapController = map.getMapController();
		mMapController.setEnabled(false);
		mOverlayManager = mMapController.getOverlayManager();
		overlay = new Overlay(mMapController);
		overlay.clearOverlayItems();
		
		// ��������� ������ �� ����
		
		 
		// ���������� ����� �� �������� ����������
		try {
			GeoPoint GP=new GeoPoint(data.getLong("latitude"), data.getLong("longitude")); 
			mMapController.setPositionAnimationTo(GP);
			mMapController.setZoomCurrent(17);
			to = new OverlayItem(GP,getResources().getDrawable(R.drawable.ic_launcher1));
			overlay.addOverlayItem(to);
			mOverlayManager.addOverlay(overlay);
			mMapController = map.getMapController();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
		case R.id.cw_description_book:
			
			Intent intent=new Intent(this, OrderDialog.class);
			intent.putExtra("id", id);
			startActivity(intent);
			
			break;
		
		}
	}
	
	

}
