package kz.smartcarwash.smartcarwash;

import kz.smartcarwash.drawerClasses.DrawerFiller;
import kz.smartcarwash.supplementaryClasses.Login;
import kz.smartcarwash.userData.User;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LoginFragment extends Fragment implements OnClickListener{
	
	private Button ok;
	private EditText login,pass;
	private TextView register;
	private final int CODE=100;
	private User U;
	private JSONObject check;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.login_fragment, container, false);
        
		return rootView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		ok=(Button) getActivity().findViewById(R.id.login_fragment_loginButton);
		ok.setOnClickListener(this);
		
		login=(EditText) getActivity().findViewById(R.id.login_fragment_login);
		pass=(EditText) getActivity().findViewById(R.id.login_fragment_password);
		
		
		register=(TextView) getActivity().findViewById(R.id.login_fragment_register);
		register.setOnClickListener(this);
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.login_fragment_loginButton:
			Login L=new Login(login.getText().toString(),pass.getText().toString());
			JSONObject j=L.getData();
			if(j!=null){
				
				try {
					if(j.getInt("status")==1){
						U=User.getInstance(getActivity());
						check=j.getJSONObject("message");
						Log.d("mylog", check.toString());
						if(check.getInt("type")<3){
						U.log_in(check);
						if(U.is_logged()){
							Fragment fragment= new MainFragment();
							FragmentManager fragmentManager = this.getFragmentManager();
						    fragmentManager.beginTransaction()
						                   .replace(R.id.content_frame, fragment)
						                   .commit();
						    DrawerFiller DF=DrawerFiller.getInstance((ActionBarActivity)this.getActivity());
						    DF.fill(U.getRole());
						}else{
							U.log_out();
							Toast.makeText(getActivity(), "Произошла ошибка", Toast.LENGTH_LONG).show();
						}
					}
						else{
							Intent intent=new Intent(getActivity(), Activate.class);
							intent.putExtra("login", login.getText().toString());
							intent.putExtra("pass", pass.getText().toString());
							startActivityForResult(intent, CODE);
						}
					}else{
						Toast.makeText(getActivity(), j.getString("message"), Toast.LENGTH_LONG).show();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				Toast.makeText(getActivity(), "Проверьте соединение с интернетом", Toast.LENGTH_LONG).show();
			}
			break;
			
		case R.id.login_fragment_register:
			Fragment fragment= new ClientRegistrationFragment();
			FragmentManager fragmentManager = this.getFragmentManager();
		    fragmentManager.beginTransaction()
		                   .replace(R.id.content_frame, fragment)
		                   .commit();
			break;
			
		
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==CODE){
			if(resultCode==-1){
				U.log_in(check);
				if(U.is_logged()){
					Fragment fragment= new MainFragment();
					FragmentManager fragmentManager = this.getFragmentManager();
				    fragmentManager.beginTransaction()
				                   .replace(R.id.content_frame, fragment)
				                   .commit();
				    DrawerFiller DF=DrawerFiller.getInstance((ActionBarActivity)this.getActivity());
				    //DF.fill(U.getRole());
				    DF.fill(1);
				}else{
					U.log_out();
					Toast.makeText(getActivity(), "Произошла ошибка", Toast.LENGTH_LONG).show();
				}
			}
		}
	}
	
	
}