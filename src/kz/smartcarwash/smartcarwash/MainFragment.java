package kz.smartcarwash.smartcarwash;

import java.util.ArrayList;

import kz.smartcarwash.supplementaryClasses.FillTables;
import kz.smartcarwash.supplementaryClasses.GetListOfCarwashes;
import kz.smartcarwash.supplementaryClasses.MyLocation;
import kz.smartcarwash.supplementaryClasses.MyLocation.LocationResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class MainFragment extends Fragment implements OnItemClickListener, OnNavigationListener{
	private ListView LV;
	private GetListOfCarwashes GLC;
	private static SQLiteDatabase myDB= null;
	private EditText mSearchEt;
	private MenuItem mSearchAction;
	//private RefreshList RF;
	
	private int cityid=1,filter=0;
	private String filterData="0";
	private double longitude=0,latitude=0;
	
	private final int SORT=1;
	
	private int sort=1;
	private FillTables FT;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.main_list_fragment, container, false);
        FT=new FillTables(getActivity());
		return rootView;
	}
	
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO Auto-generated method stub
		Log.d("mylod", "INSIDE OPTIONS MENU");
		
		
		
		inflater = getActivity().getMenuInflater();
  	  	inflater.inflate(R.menu.main, menu);
  	  	
  	  	MenuItem buttonSort=menu.findItem(R.id.action_sort);
  	  	buttonSort.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(getActivity(), new SortDialog().getClass());
				startActivityForResult(intent, SORT);
				return false;
			}
		});
  	  	ArrayAdapter<String> adapter=new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, FT.getCities());
  	  getActivity().getActionBar().setListNavigationCallbacks(adapter, this);
  	  /*
  	  	Cursor c;
		c=FT.getCursor();
		if (c!=null){
			int[] to={android.R.layout.simple_spinner_item};
			String[] from={"name"};
			SimpleCursorAdapter adapter=new SimpleCursorAdapter(getActivity().getActionBar().getThemedContext(), android.R.layout.simple_spinner_dropdown_item, c, from, to);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			getActivity().getActionBar().setListNavigationCallbacks(adapter, this);
		}
		/*
  	  	ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(getActivity(), R.array.cities, android.R.layout.simple_spinner_item);
  	  	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
  	  	//spinner.setAdapter(adapter);
  	  	
  	  	
  	  	
  	  	getActivity().getActionBar().setListNavigationCallbacks(adapter, this);
  	  	*/
  	  	final MenuItem itemSearch=(MenuItem) menu.findItem(R.id.action_search);
  	  	SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
  	  	SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
              .getActionView();
  	  	searchView.setOnQueryTextListener(new OnQueryTextListener() {
		
		@Override
		public boolean onQueryTextSubmit(String arg0) {
			// TODO Auto-generated method stub
			Log.d("mylog","HERE IS A SEARCH");
			filter=1;
			filterData=arg0;
			refreshList();
			//RF.cancel(true);
			//RF.execute();
			InputMethodManager inputMethodManager = (InputMethodManager)  getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
		    inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

			return false;
		}
		
		@Override
		public boolean onQueryTextChange(String arg0) {
			// TODO Auto-generated method stub
			return false;
		}
	});
  	  
  	ImageView closeButton = (ImageView)searchView.findViewById(R.id.search_close_btn);

    // Set on click listener
    closeButton.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
        	filter=0;
        	filterData="";
        	refreshList();
        	itemSearch.collapseActionView();
        }
    });

  	  	
		super.onCreateOptionsMenu(menu, inflater);
	}
	
	
	
	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		mSearchAction = menu.findItem(R.id.action_search);
		
		super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		getActivity().getActionBar().show();
		LV=(ListView) getActivity().findViewById(R.id.main_fragment_list);
		LV.setOnItemClickListener(this);
		
		//RF=new RefreshList();
		//GLC=new GetListOfCarwashes(1,0, 0, 0, 0);
		//LV.setAdapter(new SCWAdapter(getActivity(), extractArray(GLC.getData(), "name"), extractArray(GLC.getData(), "address"), extractArray(GLC.getData(), "img1")));
		//LV.setOnItemClickListener(this);
		setHasOptionsMenu(true);
		
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		LocationResult locationResult = new LocationResult(){
		    @Override
		    public void gotLocation(Location location){
		        //Got the location!
		    	Log.d("mylog", "LONGITUDE:"+location.getLongitude());
		    	longitude=location.getLongitude();
		    	latitude=location.getLatitude();
		    }
		};
		MyLocation myLocation = new MyLocation();
		myLocation.getLocation(getActivity(), locationResult);
		super.onResume();
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		getActivity().getActionBar().hide();
		super.onDestroy();
	}
	
	public ArrayList<String> extractArray(JSONArray j,String key){
		
		ArrayList<String> result=new ArrayList<String>();
		JSONObject temp;
		if(j!=null)
		for(int i=0;i<j.length();i++){
			try {
				temp=j.getJSONObject(i);
				result.add(temp.getString(key));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return result;
		
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
		
		try {
			Intent intent=new Intent(getActivity(), CarwashDescription.class);
			intent.putExtra("id", GLC.getData().getJSONObject(position).getInt("id"));
			startActivity(intent);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		// TODO Auto-generated method stub
		cityid=itemPosition+1;
		refreshList();
		//RF.execute();
		return false;
	}
	
	private void refreshList() {
		/*
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			GLC=new GetListOfCarwashes(cityid,longitude, latitude, filter, filterData,sort,LV);
			return null;
		}
		
		protected void onPostExecute(Void result) {
			LV.setAdapter(new SCWAdapter(getActivity(), extractArray(GLC.getData(), "name"), extractArray(GLC.getData(), "address"), extractArray(GLC.getData(), "img1")));
			
		};
		*/
		if(GLC!=null){
			Log.d("mylog","CANCEL");
			GLC.cancel();
		}
		GLC=new GetListOfCarwashes(cityid,longitude, latitude, filter, filterData,sort,LV,getActivity());
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode==SORT){
			if(resultCode==-1){
				sort=data.getIntExtra("sort", 1);
				refreshList();
				//RF.cancel(true);
				//RF.execute();
			}
		}
		
	}

}
