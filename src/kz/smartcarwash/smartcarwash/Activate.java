package kz.smartcarwash.smartcarwash;

import kz.smartcarwash.supplementaryClasses.RunScript;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Activate extends Activity implements OnClickListener {
	
	
	private TextView txt;
	private Button btn_ok, btn_cnl;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activation);
		
		btn_ok=(Button) findViewById(R.id.activation_ok);
		btn_cnl=(Button) findViewById(R.id.activation_cancel);
		txt=(TextView) findViewById(R.id.activation_txt);
		
		btn_ok.setOnClickListener(this);
		btn_cnl.setOnClickListener(this);
		
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.activation_ok:
			try{
				JSONObject js=new JSONObject();
				js.put("username", getIntent().getExtras().getString("login"));
				js.put("password", getIntent().getExtras().getString("pass"));
				js.put("code", txt.getText().toString());
				RunScript RS=new RunScript("http://smartcarwash.kz/php/verify.php", js);
				Log.d("mylog", RS.getResult());
				JSONObject j=new JSONObject(RS.getResult());
				if(j.getInt("status")==1){
					Intent intentC = new Intent();
				    setResult(RESULT_OK, intentC);
				    finish();
				}else{
					Toast.makeText(getApplicationContext(), "Неверный код", Toast.LENGTH_LONG).show();
				}
			}catch(JSONException e){
				
			}
			break;
		case R.id.activation_cancel:
			Intent intentC = new Intent();
		    setResult(RESULT_CANCELED, intentC);
		    finish();
			break;
		}
		
	}

}
