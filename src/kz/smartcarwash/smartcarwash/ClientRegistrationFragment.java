package kz.smartcarwash.smartcarwash;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import kz.smartcarwash.drawerClasses.DrawerFiller;
import kz.smartcarwash.supplementaryClasses.Register;
import kz.smartcarwash.userData.User;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ClientRegistrationFragment extends Fragment implements OnClickListener {
	
	private Button register,cancel;
	private EditText firstName,phone,pass,passConf;
	private String ph,ps,fn;
	private String photo=null;

	private ImageView img;
	private static final int SELECT_PICTURE = 1;
	private String selectedImagePath;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView = inflater.inflate(R.layout.client_register_fragment, container, false);
        
		return rootView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		register=(Button) getActivity().findViewById(R.id.client_register_registerButton);
		cancel=(Button) getActivity().findViewById(R.id.client_register_cancelButton);
		
		register.setOnClickListener(this);
		cancel.setOnClickListener(this);
		
		img=(ImageView) getActivity().findViewById(R.id.client_register_photo);
		img.setOnClickListener(this);
		
		
		firstName=(EditText) getActivity().findViewById(R.id.client_register_firstName);
		phone=(EditText) getActivity().findViewById(R.id.client_register_phone);
		pass=(EditText) getActivity().findViewById(R.id.client_registration_password);
		passConf=(EditText) getActivity().findViewById(R.id.client_registration_confirm);
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId()){
		case R.id.client_register_cancelButton:
			
			
			
			
			
			Fragment fragment= new LoginFragment();
			FragmentManager fragmentManager = this.getFragmentManager();
		    fragmentManager.beginTransaction()
		                   .replace(R.id.content_frame, fragment)
		                   .commit();
			break;
			
		case R.id.client_register_registerButton:
			ph=phone.getText().toString();
			ps=pass.getText().toString();
			fn=firstName.getText().toString();
			
			if(ph.length()==10){
				if(ps.equals(passConf.getText().toString())){
					if(ps.length()>5){
						if(fn.length()>2){
							Register Reg=new Register(ph, ps, fn,photo);
							JSONObject j=Reg.getData();
							
							try {
								if(j.getInt("status")==1){
									Toast.makeText(getActivity(), "Регистрация прошла успешно", Toast.LENGTH_SHORT).show();
									/*
									User U=User.getInstance(getActivity());
									U.log_in(j.getJSONObject("message"));
									if(U.is_logged()){
										fragment= new MainFragment();
										fragmentManager = this.getFragmentManager();
									    fragmentManager.beginTransaction()
									                   .replace(R.id.content_frame, fragment)
									                   .commit();
									    DrawerFiller DF=DrawerFiller.getInstance((ActionBarActivity)this.getActivity());
									    DF.fill(U.getRole());
									}else{
										U.log_out();
										Toast.makeText(getActivity(), "Произошла ошибка при логине", Toast.LENGTH_LONG).show();
									}
									*/
								}else{
									Toast.makeText(getActivity(), j.getString("message"), Toast.LENGTH_LONG).show();
								}
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								Toast.makeText(getActivity(), "Проверьте соединение с интернетом", Toast.LENGTH_LONG).show();
							}
							
						}else{
							Toast.makeText(getActivity(), "Введите корректное имя", Toast.LENGTH_LONG).show();
						}
					}else{
						Toast.makeText(getActivity(), "Пароль должен быть длиннее 5 символов", Toast.LENGTH_LONG).show();
					}
				}else{
					Toast.makeText(getActivity(), "Пароли не совпадают", Toast.LENGTH_LONG).show();
				}
			}
			else{
				Toast.makeText(getActivity(), "Введите корректный номер", Toast.LENGTH_LONG).show();
			}
			
			
			break;
		case R.id.client_register_photo:
			Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
			break;
		}
		
		
		
	}
	

	public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                System.out.println("Image Path : " + selectedImagePath);
                ExifInterface exif;
                try {
                	exif = new ExifInterface(selectedImagePath);
					int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
	                Log.d("EXIF", "Exif: " + orientation);
	                Matrix matrix = new Matrix();
	                if (orientation == 6) {
	                    matrix.postRotate(90);
	                }
	                else if (orientation == 3) {
	                    matrix.postRotate(180);
	                }
	                else if (orientation == 8) {
	                    matrix.postRotate(270);
	                }
	                
                	Bitmap b=decodeUri(getActivity(), selectedImageUri, 512);
                	b=Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true); // rotating bitmap
                    
					img.setImageBitmap(b);
					
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
					b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
					byte[] byteArray = byteArrayOutputStream .toByteArray();
					
					photo=Base64.encodeToString(byteArray, Base64.DEFAULT);
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
    }
	
	public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize) 
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }   

}
