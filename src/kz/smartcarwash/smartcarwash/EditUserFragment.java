package kz.smartcarwash.smartcarwash;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import kz.smartcarwash.supplementaryClasses.DisplayImage;
import kz.smartcarwash.supplementaryClasses.RunScript;
import kz.smartcarwash.userData.User;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class EditUserFragment extends Fragment implements OnClickListener{
	
	private ImageView photo;
	private EditText name,surname;
	private Button edit;
	private User user;
	private static final int SELECT_PICTURE = 1;
	private String selectedImagePath,imageString;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View rootView=inflater.inflate(R.layout.edit_user, container,false);
		return rootView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		
		photo=(ImageView) getActivity().findViewById(R.id.edit_user_image);
		name=(EditText) getActivity().findViewById(R.id.edit_user_name);
		surname=(EditText) getActivity().findViewById(R.id.edit_user_surname);
		
		edit=(Button) getActivity().findViewById(R.id.edit_user_edit);
		
		photo.setOnClickListener(this);
		edit.setOnClickListener(this);
		
		user=User.getInstance(getActivity());
		
		try {
			name.setText(user.getData().getString("firstname"));
			surname.setText(user.getData().getString("lastname"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			DisplayImage DI=new DisplayImage(photo, "http://www."+user.getData().getString("img"),getActivity());
			imageString=DI.getImageString();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.edit_user_image:
			Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
			break;
			
		case R.id.edit_user_edit:
			JSONObject jo=new JSONObject();
			try {
				jo.put("id", user.getData().getInt("id"));
				jo.put("firstname", name.getText().toString());
				jo.put("lastname", surname.getText().toString());
				jo.put("img", imageString);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			RunScript RS=new RunScript("http://www.smartcarwash.kz/php/user_edit.php", jo);
			try {
				JSONObject j=new JSONObject(RS.getResult());
				if (j.getInt("status")==1){
					user.log_in(j.getJSONObject("message"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		}
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (requestCode == SELECT_PICTURE) {
                Uri selectedImageUri = data.getData();
                selectedImagePath = getPath(selectedImageUri);
                System.out.println("Image Path : " + selectedImagePath);
                
                ExifInterface exif;
                
                try {
                	
                	exif = new ExifInterface(selectedImagePath);
					int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
	                Log.d("EXIF", "Exif: " + orientation);
	                Matrix matrix = new Matrix();
	                if (orientation == 6) {
	                    matrix.postRotate(90);
	                }
	                else if (orientation == 3) {
	                    matrix.postRotate(180);
	                }
	                else if (orientation == 8) {
	                    matrix.postRotate(270);
	                }
	                
	                
                	Bitmap b=decodeUri(getActivity(), selectedImageUri, 512);
                	b=Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true); // rotating bitmap
                    
					photo.setImageBitmap(b);
					
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
					b.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
					byte[] byteArray = byteArrayOutputStream .toByteArray();
					
					imageString=Base64.encodeToString(byteArray, Base64.DEFAULT);
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
    }
	
	public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
	public static Bitmap decodeUri(Context c, Uri uri, final int requiredSize) 
            throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);

        int width_tmp = o.outWidth
                , height_tmp = o.outHeight;
        int scale = 1;

        while(true) {
            if(width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
    }   

}
