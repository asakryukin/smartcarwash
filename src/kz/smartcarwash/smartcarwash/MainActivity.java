package kz.smartcarwash.smartcarwash;

import kz.smartcarwash.drawerClasses.DrawerFiller;
import kz.smartcarwash.supplementaryClasses.FillTables;
import kz.smartcarwash.userData.User;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity{
	private String[] mPlanetTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;

	private ActionBarDrawerToggle mDrawerToggle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		User U=User.getInstance(this);
		
		FillTables FT=new FillTables(this);
		
		/*
		if(!U.is_logged()){
		mPlanetTitles = getResources().getStringArray(R.array.sideMenuElements_NotLogged);
		}else if(U.getRole()==1){
			mPlanetTitles = getResources().getStringArray(R.array.sideMenuElements_User);
		}else{
			mPlanetTitles = getResources().getStringArray(R.array.sideMenuElements_CarWash);
		}*/
		
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
		
     // Set the adapter for the list view
        /*
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mPlanetTitles));
        // Set the list's click listener
       mDrawerList.setOnItemClickListener(new DrawerItemClickListener(this,mDrawerLayout,mDrawerList,R.id.content_frame));
       */
		DrawerFiller DF=DrawerFiller.getInstance(this);
		DF.fill(U.getRole());
		
       mDrawerToggle = new ActionBarDrawerToggle(
               this,                  /* host Activity */
               mDrawerLayout,         /* DrawerLayout object */
               R.drawable.ic_launcher1,  /* nav drawer icon to replace 'Up' caret */
               R.string.abc_action_bar_up_description,  /* "open drawer" description */
               R.string.app_name  /* "close drawer" description */
               ) {

           /** Called when a drawer has settled in a completely closed state. */
           public void onDrawerClosed(View view) {
               super.onDrawerClosed(view);
               //getActionBar().setTitle(mTitle);
           }

           /** Called when a drawer has settled in a completely open state. */
          public void onDrawerOpened(View drawerView) {
             super.onDrawerOpened(drawerView);
               //getActionBar().setTitle(mDrawerTitle);
          }
      };

       // Set the drawer toggle as the DrawerListener
      mDrawerLayout.setDrawerListener(mDrawerToggle);
       
       getActionBar().setDisplayHomeAsUpEnabled(true);
       getActionBar().setHomeButtonEnabled(true);
       getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
       getActionBar().setDisplayShowTitleEnabled(false);
       getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000033ff")));
       getActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#000055ff")));
       getActionBar().hide();
       
       Fragment fragment= new MainFragment();
       FragmentManager fragmentManager = getFragmentManager();
	    fragmentManager.beginTransaction()
	                   .replace(R.id.content_frame, fragment)
	                   .commit();

	    // Highlight the selected item, update the title, and close the drawer
	    mDrawerList.setItemChecked(0, true);
	    mDrawerLayout.closeDrawer(mDrawerList);
       
	}
	
	

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        //mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	// TODO Auto-generated method stub
    	/*
    	getMenuInflater().inflate(R.menu.main, menu);
    	Spinner s = (Spinner) menu.findItem(R.id.spinner).getActionView(); // find the spinner
    	SpinnerAdapter mSpinnerAdapter = ArrayAdapter.createFromResource(getActionBar()
    	        .getThemedContext(), R.array.cities, android.R.layout.simple_spinner_dropdown_item); //  create the adapter from a StringArray
    	s.setAdapter(mSpinnerAdapter); // set the adapter
    	*/
    	//getMenuInflater().inflate(R.menu.main, menu);
    	return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

}
