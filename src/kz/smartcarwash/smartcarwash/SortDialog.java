package kz.smartcarwash.smartcarwash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;

public class SortDialog extends Activity implements OnCheckedChangeListener, OnClickListener, android.widget.RadioGroup.OnCheckedChangeListener {
	private int mode=0;
	private Button btn_ok,btn_cancel;
	private RadioButton r1,r2,r3;
	private RadioGroup rg;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		setContentView(R.layout.sort_dialog_layout);
		super.onCreate(savedInstanceState);
		
		btn_ok=(Button) findViewById(R.id.sortD_ok);
		btn_cancel=(Button) findViewById(R.id.sortD_cancel);
		
		btn_ok.setOnClickListener(this);
		btn_cancel.setOnClickListener(this);
		
		rg=(RadioGroup) findViewById(R.id.radioGroup1);
		rg.setOnCheckedChangeListener(this);
		
		r1=(RadioButton) findViewById(R.id.radio_name);
		r2=(RadioButton) findViewById(R.id.radio_distance);
		r3=(RadioButton) findViewById(R.id.radio_price);
		
		r1.setOnClickListener(this);
		r2.setOnClickListener(this);
		r3.setOnClickListener(this);
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId()){
		
		case R.id.radio_name:
			mode=1;
			break;
		case R.id.radio_distance:
			mode=2;
			break;
			
		case R.id.radio_price:
			mode=3;
			break;
			
		case R.id.sortD_ok:
			Intent intent = new Intent();
		    intent.putExtra("sort", mode);
		    setResult(RESULT_OK, intent);
		    finish();
			break;
		case R.id.sortD_cancel:
			Intent intentC = new Intent();
		    intentC.putExtra("sort", 0);
		    setResult(RESULT_CANCELED, intentC);
		    finish();
			break;
		}
		
	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		// TODO Auto-generated method stub
		mode=checkedId+1;
		
	}

}
