package kz.smartcarwash.smartcarwash;

import org.json.JSONException;
import org.json.JSONObject;

import kz.smartcarwash.supplementaryClasses.RunScript;
import kz.smartcarwash.userData.User;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

public class OrderDialog extends Activity implements OnClickListener {
	
	private Button ok,cancel;
	private User user;
	private int id;
	private String datetime="";
	private DatePicker dp;
	private TimePicker tm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.order_dialog);
		
		id=getIntent().getExtras().getInt("id");
		
		Log.d("mylog", "ID:"+id+"");
		
		user=User.getInstance(getParent());
		
		Log.d("mylog", user.getData().toString());
		
		cancel=(Button) findViewById(R.id.book_cancel);
		cancel.setOnClickListener(this);
		ok=(Button) findViewById(R.id.book_ok);
		ok.setOnClickListener(this);
		
		dp=(DatePicker) findViewById(R.id.datePicker1);
		tm=(TimePicker) findViewById(R.id.timePicker1);
		
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		switch(v.getId()){
		
		case R.id.book_cancel:
			finish();
			break;
			
		case R.id.book_ok:
			JSONObject json=new JSONObject();
			
			try {
				json.put("client_id", user.getData().getInt("id"));
				json.put("wash_id", id);
				datetime=""+dp.getYear()+"-"+(dp.getMonth()+1)+"-"+dp.getDayOfMonth()+" "+tm.getCurrentHour()+":"+tm.getCurrentMinute();
				Log.d("mylog", datetime);
				json.put("datetime", datetime);
				json.put("service_id", "2");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			RunScript R=new RunScript("http://www.smartcarwash.kz/php/reserv_add.php", json);
			Log.d("mylog", R.getResult());
			
			try {
				json=new JSONObject(R.getResult());
				
				if(json.getInt("status")==1){
					Toast.makeText(getApplicationContext(), "Автомойка забронированна", Toast.LENGTH_LONG).show();
					finish();
				}else if(json.getInt("status")==0){
					Toast.makeText(getApplicationContext(), json.getString("message"), Toast.LENGTH_LONG).show();
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(getApplicationContext(), "Проверьте подключение к интернету", Toast.LENGTH_LONG).show();
			}
			
			break;
		
		}
		
	}

}
