package kz.smartcarwash.smartcarwash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import kz.smartcarwash.supplementaryClasses.RunScript;
import kz.smartcarwash.userData.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class WashOrdersFragment extends Fragment implements OnItemClickListener {
	
	private User user;
	private JSONObject data;
	private JSONArray orders;
	private int id=0;
	private ListView list;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View root=inflater.inflate(R.layout.wash_orders, container, false);
		return root;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		list=(ListView) getActivity().findViewById(R.id.washorders_list);
		
		user=User.getInstance(getActivity());
		data=user.getData();
		if (user.getRole()==2){
			
			
			JSONObject js=new JSONObject();
			try {
				js.put("wash_id", data.getInt("id"));
				RunScript RS=new RunScript("http://www.smartcarwash.kz/php/reserv_get.php", js);
				Log.d("mylog", RS.getResult());
				
				JSONObject results=new JSONObject(RS.getResult());
				
				if(results.getInt("status")==1){
					String[] from={"name","date","phone"};
					int[] to={R.id.woitem_name,R.id.woitem_date,R.id.woitem_phone};
					orders=results.getJSONArray("message");
					list.setAdapter(new SimpleAdapter(getActivity(), extractData(orders), R.layout.washorder_item, from, to) );
					list.setOnItemClickListener(this);
				}
				
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			JSONObject js=new JSONObject();
			
			try {
				js.put("client_id", data.getInt("id"));
				RunScript RS=new RunScript("http://www.smartcarwash.kz/php/reserv_get_client.php", js);
				Log.d("mylog", RS.getResult());
				
				JSONObject results=new JSONObject(RS.getResult());
				
				if(results.getInt("status")==1){
					String[] from={"name","date"};
					int[] to={R.id.woitem_name,R.id.woitem_date};
					orders=results.getJSONArray("message");
					list.setAdapter(new SimpleAdapter(getActivity(), extractData(orders), R.layout.washorder_item, from, to) );
					list.setOnItemClickListener(this);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	
	private List<HashMap<String,String>> extractData(JSONArray j){
		List<HashMap<String,String>> result=new ArrayList<HashMap<String,String>>();
		
		for(int i=0;i<j.length();i++){
			HashMap<String,String> t=new HashMap<String, String>();
			try {
				if(user.getRole()==2){
				t.put("name", j.getJSONObject(i).getString("firstname"));
				}else{
					t.put("name", j.getJSONObject(i).getString("name"));
				}
				t.put("date", j.getJSONObject(i).getString("datetime"));
				if(user.getRole()==2)
				t.put("phone", "8"+j.getJSONObject(i).getString("username"));
				
				result.add(t);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		return result;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		
		if(user.getRole()==2){
			try {
				Intent callIntent = new Intent(Intent.ACTION_DIAL);
				callIntent.setData(Uri.parse("tel:8"+orders.getJSONObject(position).getString("username")));
				startActivity(callIntent);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			
			try {
				Intent intent=new Intent(getActivity(), CarwashDescription.class);
				intent.putExtra("id", orders.getJSONObject(position).getInt("wash_id"));
				startActivity(intent);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/*
			Fragment fragment= new CarwashDescription();
			Bundle bundle = new Bundle();
			
				try {
					bundle.putInt("id", orders.getJSONObject(position).getInt("wash_id"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			fragment.setArguments(bundle);
			FragmentManager fragmentManager = getActivity().getFragmentManager();
		    fragmentManager.beginTransaction()
		                   .replace(R.id.content_frame, fragment)
		                   .commit();
		    */
		}
		
		
	}

}
