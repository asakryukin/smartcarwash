package kz.smartcarwash.drawerClasses;

import kz.smartcarwash.smartcarwash.EditUserFragment;
import kz.smartcarwash.smartcarwash.LoginFragment;
import kz.smartcarwash.smartcarwash.MainFragment;
import kz.smartcarwash.smartcarwash.WashOrdersFragment;
import kz.smartcarwash.userData.User;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class DrawerItemClickListener implements ListView.OnItemClickListener {
	
	private ListView mDrawerList;
	private DrawerLayout mDrawerLayout;
	private ActionBarActivity context;
	private int cfID;
	public DrawerItemClickListener(ActionBarActivity context, DrawerLayout mDrawerLayout,ListView mDrawerList,int contentframeID) {
		// TODO Auto-generated constructor stub
		this.mDrawerLayout=mDrawerLayout;
		this.mDrawerList=mDrawerList;
		this.context=context;
		this.cfID=contentframeID;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		selectItem(position);
	}
	
	private void selectItem(int position) {
	    // Create a new fragment and specify the planet to show based on position
		Fragment fragment= new MainFragment();
		User U=User.getInstance(context);
		int role = U.getRole();
		
		switch(role){
		case 0:
			switch(position){
			case 0: 
				fragment = new MainFragment();
				break;
				
			case 1: fragment = new LoginFragment();
				break;
				
			}
			break;
			
		case 1:
			switch(position){
			
			case 0: 
				fragment = new MainFragment();
				break;
				
			case 1:
				fragment = new EditUserFragment();
				break;
			case 2: fragment=new WashOrdersFragment();
				break;
			case 3: fragment = new MainFragment();
				U.log_out();
				DrawerFiller DF=DrawerFiller.getInstance(context);
				DF.fill(U.getRole());
				break;
			
			
			}
			break;
			
		case 2:
			switch(position){
			
			case 0: 
				fragment = new MainFragment();
				break;
				
			case 1: fragment=new WashOrdersFragment();
					break;
				
			case 2: fragment = new MainFragment();
				U.log_out();
				DrawerFiller DF=DrawerFiller.getInstance(context);
				DF.fill(U.getRole());
				break;
			
			
			}
			break;
		}
	    //Bundle args = new Bundle();
	    //args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
	    //fragment.setArguments(args);

	    // Insert the fragment by replacing any existing fragment
	    FragmentManager fragmentManager = context.getFragmentManager();
	    fragmentManager.beginTransaction()
	                   .replace(cfID, fragment)
	                   .commit();
	    context.getSupportFragmentManager().executePendingTransactions();
	    // Highlight the selected item, update the title, and close the drawer
	    mDrawerList.setItemChecked(position, true);
	    mDrawerLayout.closeDrawer(mDrawerList);
	}


}
