package kz.smartcarwash.drawerClasses;

import kz.smartcarwash.smartcarwash.R;
import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DrawerFiller {
	
	private static DrawerFiller instance;
	private static ActionBarActivity activity;
	
	private static String[] mPlanetTitles;
    private static DrawerLayout mDrawerLayout;
    private static ListView mDrawerList;
    
    private DrawerFiller() {
		// TODO Auto-generated constructor stub
	}
    
    public static synchronized DrawerFiller getInstance(ActionBarActivity a) {
		activity=a;
		if (instance == null) {
			
			instance = new DrawerFiller();
			
			mDrawerLayout = (DrawerLayout) a.findViewById(R.id.drawer_layout);
	        mDrawerList = (ListView) a.findViewById(R.id.left_drawer);
			
		}
		return instance;
	}
	
    public void fill(int r){
    	
    	if(r==0){
    		mPlanetTitles = activity.getResources().getStringArray(R.array.sideMenuElements_NotLogged);
    		}else if(r==1){
    			mPlanetTitles = activity.getResources().getStringArray(R.array.sideMenuElements_User);
    		}else{
    			mPlanetTitles = activity.getResources().getStringArray(R.array.sideMenuElements_CarWash);
    		}
    	
    	// Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(activity,
                   R.layout.drawer_list_item, mPlanetTitles));
        // Set the list's click listener
       mDrawerList.setOnItemClickListener(new DrawerItemClickListener(activity,mDrawerLayout,mDrawerList,R.id.content_frame));
       
    }

}
