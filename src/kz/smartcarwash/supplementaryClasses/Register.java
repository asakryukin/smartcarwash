package kz.smartcarwash.supplementaryClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class Register {

	private String username,password,name,photo;
	private JSONObject js=null;
	
	
	public Register(String username,String password,String name,String photo) {
		// TODO Auto-generated constructor stub
		this.username=username;
		this.password=password;
		this.name=name;
		this.photo=photo;
		BackRegister BR=new BackRegister();
		try {
			js=BR.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	}
	public JSONObject getData(){
		return js;
	}
	private class BackRegister extends AsyncTask<Void, Void, JSONObject>{
		private JSONObject json;
		@Override
		protected JSONObject doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpResponse response;
			StringBuilder builder=new StringBuilder();
			try {
				HttpClient httpclient = new DefaultHttpClient();
				
				HttpPost httppost = new HttpPost("http://www.smartcarwash.kz/php/client_register.php");
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				
				JSONObject js=new JSONObject();
				js.put("username", username);
				js.put("password", password);
				js.put("firstname", name);
				
				if(photo!=null)
					js.put("img", photo);
		        nameValuePairs.add(new BasicNameValuePair("data", js.toString()));
		        Log.d("mylog", js.toString());
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
				response = httpclient.execute(httppost);
				String url_response=EntityUtils.toString(response.getEntity());
				Log.d("mylog", url_response);
				json=new JSONObject(url_response);
				
				//Log.d("mylog", response.toString());
				//Log.d("mylog", response.getEntity().getContent().toString());
				
			}catch(IOException e)
			{
				Log.d("mylog", e.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return json;
		}
		
	}

}
