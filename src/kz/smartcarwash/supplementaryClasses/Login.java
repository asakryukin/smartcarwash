package kz.smartcarwash.supplementaryClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class Login {
	
	private String username,password;
	private JSONObject js=null;
	
	public Login(String username,String password) {
		// TODO Auto-generated constructor stub
		this.username=username;
		this.password=password;
		BackLogin BL=new BackLogin();
		try {
			js=BL.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
	}
	public JSONObject getData(){
		return js;
	}
	private class BackLogin extends AsyncTask<Void, Void, JSONObject>{
		private JSONObject json;
		@Override
		protected JSONObject doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpResponse response;
			StringBuilder builder=new StringBuilder();
			try {
				HttpClient httpclient = new DefaultHttpClient();
				
				HttpPost httppost = new HttpPost("http://www.smartcarwash.kz/php/login.php");
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				
				JSONObject js=new JSONObject();
				js.put("username", username);
				js.put("password", password);
				
		        nameValuePairs.add(new BasicNameValuePair("data", js.toString()));
		        Log.d("mylog", js.toString());
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				response = httpclient.execute(httppost);
				String url_response=EntityUtils.toString(response.getEntity());
				Log.d("mylog", url_response);
				json=new JSONObject(url_response);
				
				//Log.d("mylog", response.toString());
				//Log.d("mylog", response.getEntity().getContent().toString());
				
			}catch(IOException e)
			{
				Log.d("mylog", e.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return json;
		}
		
	}

}
