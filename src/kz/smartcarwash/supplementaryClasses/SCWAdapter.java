package kz.smartcarwash.supplementaryClasses;

import java.util.ArrayList;

import kz.smartcarwash.smartcarwash.R;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SCWAdapter extends BaseAdapter{
	
	private ArrayList<String> names,addresses,urls;
	private Activity activity;
	private LayoutInflater inflater;
	
	public SCWAdapter(Activity context, ArrayList<String> names, ArrayList<String> addresses,ArrayList<String> urls) {
		// TODO Auto-generated constructor stub
		this.activity=context;
		this.names=names;
		this.addresses=addresses;
		this.urls=urls;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return names.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.main_list_item, null);
        TextView name = (TextView) convertView.findViewById(R.id.main_list_item_name);
        TextView address = (TextView) convertView.findViewById(R.id.main_list_item_address);
        ImageView img=(ImageView) convertView.findViewById(R.id.main_list_item_photo);
        
        name.setText(names.get(position));
        address.setText(addresses.get(position));
        DisplayImage DI=new DisplayImage(img, "http://www."+urls.get(position),activity);
		return convertView;
	}

}
