package kz.smartcarwash.supplementaryClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

public class RunScript {
	
	private String url;
	private JSONObject arguments;
	private String result="";
	
	public RunScript(String url,JSONObject arguments) {
		// TODO Auto-generated constructor stub
		this.url=url;
		this.arguments=arguments;
		DoUrl DU=new DoUrl();
		try {
			DU.execute().get();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String getResult(){
		return result;
	}
	
	private class DoUrl extends AsyncTask<Void, Void, String>{

		@Override
		protected String doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpResponse response;
			StringBuilder builder=new StringBuilder();
			
			
			try{
				HttpClient httpclient=new DefaultHttpClient();
				HttpPost httppost=new HttpPost(url);
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
				
				nameValuePairs.add(new BasicNameValuePair("data", arguments.toString()));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
				response = httpclient.execute(httppost);
				result=EntityUtils.toString(response.getEntity());
				Log.d("mylog", result);
				
			}catch(IOException e){
				
			}
			
			return result;
		}
		
	}

}
