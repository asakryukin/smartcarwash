package kz.smartcarwash.supplementaryClasses;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter{

	private int mGalleryItemBackground;
	private Context mContext;
	private Activity activity;
	private String[] urls;
	
	public ImageAdapter(Activity activity, String[] strings) {
		this.activity=activity;
		mContext = activity.getApplicationContext();
		urls=strings;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return urls.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return urls[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ImageView image=new ImageView(mContext);
		DisplayImage DI=new DisplayImage(image, urls[position],activity);
		
		
		return image;
	}

}
