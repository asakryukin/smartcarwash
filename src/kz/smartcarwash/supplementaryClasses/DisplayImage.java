package kz.smartcarwash.supplementaryClasses;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import kz.smartcarwash.userData.BitmapCache;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.ImageView;

public class DisplayImage {
	
	private ImageView img;
	private String url;
	private String imageString="";
	private BitmapCache BC;

	public DisplayImage(ImageView img,String url,Activity a) {
		// TODO Auto-generated constructor stub
		this.img=img;
		this.url=url;
		
		BC=BitmapCache.getInstance(a);
		Bitmap b=BC.getImage(url);
		
		if(b==null){
			LoadImage LI=new LoadImage();
			LI.execute(url);
		}else{
			this.img.setImageBitmap(b);
		}
		
		
	}
	
	public String getImageString(){
		return imageString;
	}
	
	private class LoadImage extends AsyncTask<String, String, Bitmap> {
			private Bitmap bitmap=null;
			
			protected Bitmap doInBackground(String... args) {
					try {
						bitmap = BitmapFactory.decodeStream((InputStream)new URL(url).getContent());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return bitmap;
					}
			
			protected void onPostExecute(Bitmap image){
				if(image != null){
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
					image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
					byte[] byteArray = byteArrayOutputStream .toByteArray();
					
					imageString=Base64.encodeToString(byteArray, Base64.DEFAULT);
					img.setImageBitmap(image);
					BC.addImage(url, image);
					}
				}
			}
}
