package kz.smartcarwash.supplementaryClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

public class FillTables {
	private static SQLiteDatabase myDB= null;
	private static Cursor c;
	
	public FillTables(Activity a) {
		// TODO Auto-generated constructor stub
		myDB = a.openOrCreateDatabase("SmartCarWash", 0, null);
		myDB.execSQL("create table if not exists Cities ("
		          + "_id integer primary key,"
		          + "name text"
		          + ");");
		c=myDB.rawQuery("SELECT * FROM Cities", null);
		
		if(c.getCount()<=0){
			FillCities FC=new FillCities();
			try {
				FC.execute().get();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public Cursor getCursor(){
		if (c.getCount()>0){
			c.moveToFirst();
			return c;
		}else{
			return null;
		}
	}
	
	public ArrayList<String> getCities(){
		ArrayList<String> result=new ArrayList<String>();
		getCursor();
		for(int i=0;i<c.getCount();i++){
			result.add(c.getString(c.getColumnIndex("name")));
			c.moveToNext();
		}
		return result;
	}
	
	private class FillCities extends AsyncTask<Void, Void, Void>{

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpResponse response;
			StringBuilder builder=new StringBuilder();
			try {
				HttpClient httpclient = new DefaultHttpClient();
				
				HttpPost httppost = new HttpPost("http://www.smartcarwash.kz/php/city_get.php");
				
				response = httpclient.execute(httppost);
				String url_response=EntityUtils.toString(response.getEntity());
				Log.d("mylog", url_response);
				JSONObject json=new JSONObject(url_response);
				
				if(json.getInt("status")==1){
					JSONArray ja=json.getJSONArray("message");
					for(int i=0;i<ja.length();i++){
						myDB.execSQL("INSERT INTO Cities"
							     + " (_id, name)"
							     + " VALUES ("+ja.getJSONObject(i).getInt("city_id")
							     +",'"
							     +ja.getJSONObject(i).getString("city_name")+"'"+");");
					}
				}
				
				//Log.d("mylog", response.toString());
				//Log.d("mylog", response.getEntity().getContent().toString());
				
			}catch(IOException e)
			{
				Log.d("mylog", e.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
	}

}
