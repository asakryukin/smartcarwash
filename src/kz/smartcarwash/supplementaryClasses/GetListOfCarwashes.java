package kz.smartcarwash.supplementaryClasses;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ListView;

public class GetListOfCarwashes {
	
	private double longitude=0,latitude=0;
	private int filter=0,cityid;
	private int sort;
	private String filterData="0";
	private JSONArray js=null;
	private ListView LV;
	private Activity context;
	private GetList GL;
	public GetListOfCarwashes(int cityid, double longitude,double latitude,int filter,String filterData,int sort, ListView LV, Activity context) {
		// TODO Auto-generated constructor stub
		
		this.longitude=longitude;
		this.latitude=latitude;
		this.filter=filter;
		this.filterData=filterData;
		this.cityid=cityid;
		this.sort=sort;
		this.LV=LV;
		this.context=context;
		
		GL=new GetList();
		GL.execute();
		/*
		try {
			GL.execute();
			//if (GL!=null){}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	}
	
	public void cancel(){
		GL.cancel(true);
		
	}
	
	public JSONArray getData(){
		return js;
	}
	
	private class GetList extends AsyncTask<Void, Void, JSONObject>{
		private JSONObject json;
		private HttpPost httppost;
		@Override
		protected JSONObject doInBackground(Void... params) {
			// TODO Auto-generated method stub
			HttpResponse response;
			StringBuilder builder=new StringBuilder();
			try {
				
				JSONObject js=new JSONObject();
				js.put("longitude", longitude);
				js.put("latitude", latitude);
				js.put("filter", filter);
				js.put("filterData", filterData);
				js.put("filterData", filterData);
				js.put("addr_city", cityid);
				js.put("sort", sort);
				Log.d("mylog", js.toString());
				
				HttpClient httpclient = new DefaultHttpClient();
				HttpParams httpParameters = httpclient.getParams();
				httpParameters.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
				HttpConnectionParams.setConnectionTimeout(httpParameters, 3000);
				HttpConnectionParams.setSoTimeout(httpParameters, 3000);
				HttpConnectionParams.setTcpNoDelay(httpParameters, true);
				httppost = new HttpPost("http://www.smartcarwash.kz/php/wash_getlist.php");
				
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		        nameValuePairs.add(new BasicNameValuePair("data", js.toString()));
		        
		        //httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
				response = httpclient.execute(httppost);
				String url_response=EntityUtils.toString(response.getEntity());
				Log.d("mylog","CARWASHES:"+ url_response+" end");
				json=new JSONObject(url_response);
				
				//Log.d("mylog", response.toString());
				//Log.d("mylog", response.getEntity().getContent().toString());
				
			}catch(IOException e)
			{
				Log.d("mylog", e.toString());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return json;
		}
		
		@Override
		protected void onPostExecute(JSONObject result) {
			// TODO Auto-generated method stub
			try {
				LV.setAdapter(null);
				LV.setAdapter(new SCWAdapter(context, extractArray(result.getJSONArray("message"), "name"), extractArray(result.getJSONArray("message"), "address"), extractArray(result.getJSONArray("message"), "img1")));

				if(result.getInt("status")==1){
					js=result.getJSONArray("message");
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch(NullPointerException e ){
				
			}
			
			super.onPostExecute(result);
		}
		
		@Override
		protected void onCancelled() {
			// TODO Auto-generated method stub
			if (httppost!=null)
			httppost.abort();
			super.onCancelled();
		}
		
	}
	
	public ArrayList<String> extractArray(JSONArray j,String key){
		
		ArrayList<String> result=new ArrayList<String>();
		JSONObject temp;
		if(j!=null)
		for(int i=0;i<j.length();i++){
			try {
				temp=j.getJSONObject(i);
				result.add(temp.getString(key));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return result;
		
	}

}
