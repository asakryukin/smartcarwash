package kz.smartcarwash.userData;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class User {
	private static User instance;
	private static Activity activity;
	
	private static boolean logged=false;
	private static int id=0;
	private static int role=0;
	private static JSONObject jsonData=null;
	
	private static SQLiteDatabase myDB= null;
	private static Cursor c;
	
	private User() {
		// TODO Auto-generated constructor stub
	}
	
	public static synchronized User getInstance(Activity a) {
		activity=a;
		if (instance == null) {
			
			instance = new User();
			
			if(checkDB()){
				
				try {
					c=myDB.rawQuery("SELECT * FROM Active", null);
					c.moveToFirst();
					jsonData=new JSONObject(c.getString(c.getColumnIndex("json")));
					logged=true;
					
					id=c.getInt(c.getColumnIndex("_id"));
					role=c.getInt(c.getColumnIndex("role"));
					Log.d("mylog", "ID:"+id+" role"+role);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logged=false;
					log_out();
				}
			}
			
		}
		return instance;
	}
	
	public static boolean is_logged(){
		return logged;
	}
	
	public boolean log_in(JSONObject json){
		
		if(is_logged()){
			log_out();
			
			try {
				
				id=json.getInt("id");
				role=json.getInt("type");
				jsonData=json;
				insertDB();
				logged=true;
				Log.d("mylog", "Login ID:"+id+" role"+role);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log_out();
				return false;
			}
			
			return true;
		}else{
			try {
				id=json.getInt("id");
				role=json.getInt("type");
				jsonData=json;
				insertDB();
				logged=true;
				Log.d("mylog", "Login ID:"+id+" role"+role);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				log_out();
				return false;
			}
			
			
			return true;
		}
	}
	
	public static boolean log_out(){
			id=0;
			jsonData=null;
			role=0;
			myDB.execSQL("DELETE FROM Active WHERE _id>0");
			logged=false;
			return true;
		
	}
	
	private static boolean checkDB(){
		myDB = activity.openOrCreateDatabase("SmartCarWash", 0, null);
		myDB.execSQL("create table if not exists Active ("
		          + "_id integer primary key,"
		          + "role integer,"
		          + "json text"
		          + ");");
		c=myDB.rawQuery("SELECT * FROM Active", null);
		
		if (c.getCount()>0){
			return true;
		}else{
			return false;
		}
		
	}
	
	private boolean insertDB(){
		myDB.execSQL("INSERT INTO Active"
			     + " (_id, role,json)"
			     + " VALUES ("+id+","
			     +role+",'"
			     +jsonData+"'"+");");
		c=myDB.rawQuery("SELECT * FROM Active", null);
		if (c.getCount()>0){
			return true;
		}else{
			return false;
		}
	}
	public int getRole(){
		return role;
	}
	
	
	public JSONObject getData(){
		return jsonData;
	}
}
