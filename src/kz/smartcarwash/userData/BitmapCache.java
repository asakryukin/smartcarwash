package kz.smartcarwash.userData;

import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class BitmapCache {
	
	private static LruCache<String, Bitmap> cache;
	private static BitmapCache instance;
	private static Activity activity;
	
	private BitmapCache(){
		cache=new LruCache<String, Bitmap>(10);
	}
	
	public static synchronized BitmapCache getInstance(Activity a){
		
		activity=a;
		if (instance == null) {
			
			instance = new BitmapCache();
			
		}
		return instance;
		
	}
	
	public static void addImage(String url,Bitmap b){
		cache.put(url, b);
	}
	
	public static Bitmap getImage(String url){
		return cache.get(url);
		
	}
}
